import json
import re 

from reader import Reader

#[BOT]
RE_DFTA = re.compile(r'^[^\s]*\s\[DFTA\] (.*)$', re.MULTILINE)
# Logging out of the caves

#RE_SIMPAUSED = re.compile(r'^[^\s]*\sSim paused$', re.MULTILINE)
RE_DISCONNECTED = re.compile(r'^[^\s]*\s\[Shard\] \(([^\)]*)\) disconnected from .*$', re.MULTILINE)

RE_SAY = re.compile(r'^[^\s]*\s\[Say\] (.*)$', re.MULTILINE)
RE_WHISPER = re.compile(r'^[^\s]*\s\[Whisper\] (.*)$', re.MULTILINE)

class ServerLogsReader(list):
    def __init__(self, path_master_log, path_cave_log, path_chat_log=None):
        self.master_log = Reader(path_master_log)
        if path_cave_log:
            self.caves_log = Reader(path_cave_log)
        else:
            self.checkLogCave = lambda : None

        if path_chat_log:
            self.chat_log = Reader(path_chat_log)
        else:
            print("CHAT LOG FAILURE IN ServerLogsReader")

    def checkLog(self, data, isCave):
        for msg in re.findall(RE_DFTA, data):
            if msg: 
                try:
                    x = json.loads(msg)
                except Exception as e:
                    msg = msg.replace("\\'", "'")
                    try:
                        x = json.loads(msg)
                    except Exception as e:
                        print("JSON FAILED:")
                        print(msg)
                        print("Error:")
                        print(e)
                        print("--------------")
                        continue
                x['cavelog'] = isCave
                self.append(x)
                #print(x)
        for msg in re.findall(RE_DISCONNECTED, data):
            #print("msg")
            if msg: 
                x = {"category": "shard_disconnected", "userid": msg}
                self.append(x)
                #print(x)
    
    def checkCL(self, data):
        for msg in re.findall(RE_SAY, data):
            if msg:
                x = {"category": "say_chat", "msg": msg}
                self.append(x)
                #print(x)
                #print("appended x")

    def checkLogMaster(self):
        data = self.master_log.read()
        #print("data = :" + str(data))
        self.checkLog(data, False)

    def checkLogCave(self):
        data = self.caves_log.read()
        self.checkLog(data, True)

    def checkChat(self):
        data = self.chat_log.read()
        self.checkCL(data)

    def check(self):
        # Clear the list
        self.clear()
        self.checkLogMaster()
        self.checkLogCave()
        self.checkChat()
