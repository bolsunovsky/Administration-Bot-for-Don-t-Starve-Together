import discord
import random
import settings 
from utils import strDecompose
from distutils.dir_util import copy_tree, _path_created
import asyncio
from shutil import rmtree
import subprocess
import shutil, errno


from os import makedirs
from os.path import isfile
from pathlib import Path


class Bot(discord.Client):
    def __init__(self):
        super(Bot, self).__init__()
        
        
        self.locked  = False
        self.publicCommandList = {
        
            'hello' : self.cmdHelloWorld,
            'dst' : self.cmdServerInfo,
            'dft' : self.cmdServerInfo,
            'дст' : self.cmdServerInfo,
            'help' : self.cmdHelp,
            'man' : self.cmdHelp,
            'кинь' : self.table,
            'верни' : self.putBack,
            'ура' : self.happy,
            'сохранить' : lambda: asyncio.create_task(self.backupSave()),
            'возобновить' : lambda: asyncio.create_task(self.loadSave()),
        }
        self.publicCommandMemes = {
            'meow' : self.cmdMeow,
            'woof' : self.cmdWoof,
            'birb' : self.cmdBirb,
            'bird' : self.cmdBirb,
            'quack' : self.cmdBirb,
        }


    def copyFile(self, src, dst):
        try:
            shutil.copytree(src, dst)
        # Depend what you need here to catch the problem
        except OSError as exc:
            # File already exist
            if exc.errno == errno.EEXIST:
                shutil.copy(src, dst)
            # The dirtory does not exist
            if exc.errno == errno.ENOENT:
                shutil.copy(src, dst)
            else:
                raise

             
    async def loadSave(self):
        if self.locked: 
            print("saving/loading is in process: locked (clops.py)")
            return

        self.locked = True
        
        masterSavePath = '/home/dstadmin/.klei/DoNotStarveTogether/MyDediServer/Master/save'
        caveSavePath = '/home/dstadmin/.klei/DoNotStarveTogether/MyDediServer/Caves/save'
        serverPath = '/home/dstadmin/.klei/DoNotStarveTogether/MyDediServer'

        #await asyncio.sleep(5)
        if not Path("/home/dstadmin/.klei/DoNotStarveTogether/MyDediServer/Master/save").is_dir():
            makedirs('/home/dstadmin/.klei/DoNotStarveTogether/MyDediServer/Master/save')
        if not Path("/home/dstadmin/.klei/DoNotStarveTogether/MyDediServer/Caves/save").is_dir():
            makedirs('/home/dstadmin/.klei/DoNotStarveTogether/MyDediServer/Caves/save')
        rmtree('/home/dstadmin/.klei/DoNotStarveTogether/MyDediServer/Master/save')
        rmtree('/home/dstadmin/.klei/DoNotStarveTogether/MyDediServer/Caves/save')
        
        #makedirs('/home/dstadmin/.klei/DoNotStarveTogether/MyDediServer')
        #subprocess.call(['cp', '-a', "/home/dstadmin/backup_dst", "/home/dstadmin/.klei/DoNotStarveTogether/MyDediServer"])
        await asyncio.sleep(2)
        self.copyFile("/home/dstadmin/backup_dst/Caves/save", "/home/dstadmin/.klei/DoNotStarveTogether/MyDediServer/Caves/save")
        self.copyFile("/home/dstadmin/backup_dst/Master/save", "/home/dstadmin/.klei/DoNotStarveTogether/MyDediServer/Master/save")
        await asyncio.sleep(2)
        
        
        #copy_tree("/home/dstadmin/backup_dst/", "/home/dstadmin/.klei/DoNotStarveTogether/MyDediServer/")
        self.passToDST("!resetServer")
        #message.channel.send("Загрузил последнюю копию мира. Перезагрузите сервер введя c_reset() в консоль во время игры")
        #self.response = "Загрузил последнюю копию мира"
        await asyncio.sleep(60)
        #self.passToDST("!backupServer")
        self.locked = False



    async def backupSave(self):
        if self.locked: 
            print("saving/loading is in process: locked (clops.py)")
            return
        serverPath = '/home/dstadmin/.klei/DoNotStarveTogether/MyDediServer'
        backupPath = '/home/dstadmin/backup_dst'

        self.locked = True

        #await asyncio.sleep(30)

        self.passToDST("!backupServer")
        await asyncio.sleep(5)
        #await message.channel.send(embed=self.response)
        #if not Path('/home/dstadmin/backup_dst/Master/save').is_dir():
        #    makedirs('/home/dstadmin/backup_dst/Master/save')
        #if not Path('/home/dstadmin/backup_dst/Cave/save').is_dir():
        #    makedirs('/home/dstadmin/backup_dst/Cave/save')
        #rmtree('/home/dstadmin/backup_dst/Master/save')
        #_path_created = {}
        #rmtree('/home/dstadmin/backup_dst/Cave/save')
        #_path_created = {}
        #self.copyFile("/home/dstadmin/.klei/DoNotStarveTogether/MyDediServer/Master/save","/home/dstadmin/backup_dst/Master/save")
        #self.copyFile("/home/dstadmin/.klei/DoNotStarveTogether/MyDediServer/Caves/save", "/home/dstadmin/backup_dst/Caves/save")
        _path_created = {}
        copy_tree("/home/dstadmin/.klei/DoNotStarveTogether/MyDediServer", "/home/dstadmin/backup_dst")
        #copy_tree("/home/dstadmin/.klei/DoNotStarveTogether/MyDediServer/Master/save","/home/dstadmin/backup_dst/Master/save")
        #copy_tree("/home/dstadmin/.klei/DoNotStarveTogether/MyDediServer/Caves/save", "/home/dstadmin/backup_dst/Caves/save")
        #self.passToDST("!backupServer")
        await asyncio.sleep(5)
        #self.passToDST("!restartServer")
        #await asyncio.sleep(70)

        #message.channel.send("Запасная копия сохранена (используйте команду .возобновить чтобы загрузить последнюю версию мира)")
        #self.response = "Запасная копия сохранена (используйте команду .возобновить чтобы загрузить последнюю версию мира)"
        
        self.locked = False

    # Alias for Placeholder
    async def cmdHelloWorld(self):
        self.response = random.choice(settings.hello_responses)

    def table(self):
        self.response = "(╯°□°）╯︵ ┻━┻"

    def putBack(self):
        self.response = "┬─┬ ノ( ゜-゜ノ)"

    def happy(self):
        self.response = "ヽ(^o^)ノ"

    # Memes hello world
    # Creates embed response
    async def sendRandomImage(self, image_type, image_count):
        rand = random.randint(1, image_count)
        img_url = f"https://nadeko-pictures.nyc3.digitaloceanspaces.com/{image_type}/{rand:03d}.png"
        self.response = discord.Embed()
        self.response.set_image(url=img_url)

    # Shows random cat
    async def cmdMeow(self):
        self.sendRandomImage("cats", 772)
    
    # Shows random dog
    def cmdWoof(self):
        self.sendRandomImage("dogs", 749)

    # Shows random bird
    def cmdBirb(self):
        self.sendRandomImage("birds", 577)

    # Shows cat as princess Leia
    # TODO make better help
    def cmdHelp(self):
        self.response = f"""**Public commands**
    `{settings.prefix}hello`  Prints hello world
    `{settings.prefix}dst`  Show server info and who is online. You can also check #whois
    `{settings.prefix}dft`  Alias for dst cmd
    `{settings.prefix}дст`  Alias for dst cmd
    `{settings.prefix}help`  Prints this help
    `{settings.prefix}man`  Alias for help cmd

**Public commands that you can use at #meme**
    `{settings.prefix}meow`  Show "random" :cat: image
    `{settings.prefix}woof`  Show "random" :dog: image
    `{settings.prefix}birb`  Show "random" :bird: image
    `{settings.prefix}bird`  Alias for birb cmd
    `{settings.prefix}quack` Alias for birb cmd
"""
    
    # Checks if the server is active or not. 
    def cmdServerInfo(self):
        try:
            self.response = self.last_msg
        except AttributeError:
            self.response = "Try it later."
        

    # Return the method from the  command list
    def _findInCommandList(self, commandList):
        for _cmd in commandList:
            if self.message.startswith(_cmd):
                return commandList[_cmd]
        return None


    def _getCommand(self):
        cmd = self._findInCommandList(self.publicCommandList )
        if cmd is not None:
            return cmd
        # Check if it is memes channel
        if self.server_name == 'memes':
            cmd = self._findInCommandList(self.publicCommandMemes)
        if cmd is not None:
            return cmd
        # default:
        return lambda: None

    def passToDST(self, message):
        discord_log = open("/home/dstadmin/dontstarvetogether_dedicated_server/mods/dft_announcer_lite/discord_log.txt", "w")
                
        if isinstance(message, str):
            
            print(message)
            print(type(message)) 
            discord_log.write(message + "\n")
        
        else:
             
            if not message.author.bot:
                self.author_name = message.author.name
                print(message.content)
                print(type(message.content))
                
                discord_log.write("%s: %s \n" % (message.author.name, message.content))

            #except AttributeError:
             #   return print("")
                     #print("Author not defined, deerclops.py")

        discord_log.close()

    async def parseCommands(self, message):
        # Optimize command parsing
        # Check if the first character is equal to command starter
        if not message.content:
            return

        if not message.content.startswith(settings.prefix):

            if message.channel.name == 'kvas-dst':
                self.passToDST(message)

 #       if message.content.startswith("!"): 

        if message.channel.guild.id != settings.guild_id:
            print("Bad guild id")
            return 
        self.message = message.content[len(settings.prefix):]
        self.channel = message.channel
        self.server_name = message.channel.name
        try:
            self.author_name = message.author.name
            self.author_roles = [y.name for y in message.author.roles]
        except AttributeError: 
            self.author_roles = ["Admin"] if message.author.id in MAXWELLS else []
        # Result string and command
        self.response = None
        command = self._getCommand()
        # Run the command 
        #asyncio.create_task(command())
        command()
        # Send the response
        if self.response != None:
            # Decompose the response and get the result 
            if isinstance(self.response, str):
                for response in strDecompose(self.response):
                    await message.channel.send(response)
            else:
                await message.channel.send(embed=self.response)


