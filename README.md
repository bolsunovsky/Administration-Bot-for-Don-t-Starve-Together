# Administrative DST Bot for Discord
Discord bot that prints current status in Don't Starve Together server. The bot shows the current day, season, day phase, and total players. For each player, it also displays their character, name and played days.

### Example
![Discord Bot Example](https://i.imgur.com/pS5AbjB.png "Discord Bot Example")

### Supports 2-way Chat Communication
![Discord Bot Example](https://i.imgur.com/NZw4YDs.png "Chat Example")

### Requirements
- Linux
- Python 3.6 or newer
- Discord library for Python `pip3 install discord`
- Server mod [to be added later]
### Installation
- [Get Discord token](https://discordapp.com/developers/)
- Download this mod
- Edit settings in `settings.py`
---
You can run the script with `./run.sh`. You can also specify the core on which you wish to run this bot with adding an argument that coresponds to the core id: `./run.sh 7`
